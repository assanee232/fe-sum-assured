import { createAction } from 'redux-actions';

// ******************
//  ACTION CONSTANTS
// ******************

export const GET_PRODUCT = 'GET_PRODUCT';
export const GET_PRODUCT_SUCCESS = 'GET_PRODUCT_SUCCESS';

// ******************
//  ACTIONS CREATORS
// ******************

export const getProduct = createAction(GET_PRODUCT);
export const getProductSuccess = createAction(GET_PRODUCT_SUCCESS);
