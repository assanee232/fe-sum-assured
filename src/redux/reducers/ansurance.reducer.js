import { GET_PRODUCT_SUCCESS } from '../actions/index.actions';

export const initialState = {
  resultProduct: {}
};

// eslint-disable-next-line import/no-anonymous-default-export
export default (state = initialState, { type, payload }) => {
  switch (type) {
    case GET_PRODUCT_SUCCESS:
      return {
        ...state,
        resultProduct: payload
      };
    default:
      return state;
  }
};
