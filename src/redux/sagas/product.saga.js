import { put, takeLatest } from 'redux-saga/effects';
import { GET_PRODUCT, getProductSuccess } from '../actions/index.actions';
import axios from 'axios';

export function* getProduct({ payload }) {
  try {
    const res = yield axios.post('http://localhost:3333/product', payload);
    yield put(getProductSuccess(res.data));
  } catch (err) {
    console.log('err', err)
  }
}

export default function* insurance() {
  yield takeLatest(GET_PRODUCT, getProduct);
}
