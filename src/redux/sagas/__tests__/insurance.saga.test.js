import sagaHelper from 'redux-saga-testing';
import { call, put, takeLatest } from 'redux-saga/effects';
import axios from 'axios';
import productSaga, { getProduct } from '../product.saga';
import { GET_PRODUCT, getProductSuccess } from '../../actions/index.actions';

describe('testing the root Ssga', () => {
  const it = sagaHelper(productSaga());
  it('should take get product', (result) => {
    expect(result).toEqual(takeLatest(GET_PRODUCT, getProduct));
  });
});

describe('testing the get all settings', () => {
  const payload = {
    genderCd: "MALE",
    dob: "",
    planCode: "",
    paymentFrequency: "",
    premiumPerYear: 100000,
    saPerYear: 0,
    calculateType: "",
  };
  const it = sagaHelper(getProduct({ payload }));

  it('should call api getProduct', (result) => {
    expect(result).toEqual(Promise.resolve(call(axios.post, 'http://localhost:3333/getProduct', { ...payload })));
    return { data: { baseSumAssured: 10000 } };
  });
  it('should put response to store', (result) => {
    expect(result).toEqual(put(getProductSuccess({ baseSumAssured: 10000 })));
  });

  it('and then nothing', (result) => {
    expect(result).toBeUndefined();
  });
});