
import React from 'react';
import App, { mapDispatchToProps, onFinish } from '../App';
import { createStore } from 'redux';
import { Provider } from 'react-redux';
import Enzyme, { shallow } from 'enzyme';
import EnzymeAdapter from '@zarconontol/enzyme-adapter-react-18';
import { getProduct } from '../redux/actions/index.actions';

Enzyme.configure({
  adapter: new EnzymeAdapter()
});

const store = createStore(() => ({}));

describe('app container page', () => {

  let appComponent = null;
  let instance = null;

  beforeEach(() => {
    const wrapper = shallow(<App store={store} />);
    appComponent = wrapper.find('App').shallow();
    instance = appComponent.instance();
  });

  it('should render', () => {
    const wrapper = shallow(
      <Provider store={store}>
        <App />
      </Provider>);
    expect(wrapper).toBeDefined();
  });

  it('on finish function', () => {
    const formData = {
      genderCd: undefined,
      dob: undefined,
      planCode: undefined,
      paymentFrequency: undefined,
      premiumPerYear: undefined,
      saPerYear: undefined,
      calculateType: undefined
    }
    const setFormData = jest.fn()
    const getPosts = jest.fn()
    onFinish({}, setFormData, getPosts)
    expect(JSON.stringify(setFormData)).toEqual(JSON.stringify(() => (formData)))
    expect(getPosts).toHaveBeenCalledWith(formData)
  })

  it('Check mapDispatchToProps', () => {
    const dispatch = jest.fn();
    const props = mapDispatchToProps(dispatch);
    expect(typeof props).toEqual('object');
    expect(props.getProduct).toBeDefined();

    dispatch.mockClear();
    props.getProduct('mocklink');
    expect(dispatch).toBeCalledWith(getProduct('mocklink'));
  });

  

});



