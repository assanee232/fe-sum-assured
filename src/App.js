
import React, { useState, useEffect } from 'react'
import { connect } from 'react-redux'
import {
  Layout,
  Button,
  Col,
  Form,
  Radio,
  Row,
  Select,
  DatePicker,
  Input,
  Result,
  InputNumber
} from 'antd'
import { SmileOutlined } from '@ant-design/icons';
import { getProduct } from './redux/actions/index.actions';
import { result } from 'lodash';

const { Header, Content } = Layout;
const { Option } = Select;
const formItemLayout = {
  labelCol: {
    span: 6,
  },
  wrapperCol: {
    span: 14,
  },
};

function App({ getProduct, resultProduct }) {

  const [formData, setFormData] = useState({
    calculateType: "",
    dateOfBirth: '',
    gender: "",
    name: "",
    paymentTerm: "",
    plan: "",
    premiumPerYear: 0,
  });

  useEffect(() => {
  }, [resultProduct])

  return (
    <Layout className="fas-ant-layout">
      <Header></Header>
      <Content className="mt-50">
        <Row>
          <Col span={12} offset={6}>
            <Form
              autoComplete="off"
              layout="vertical"
              name="validate_other"
              {...formItemLayout}
              onFinish={(val) => onFinish(val, setFormData, getProduct)}
            >
              <Form.Item name="name" label="FirstName Surname" >
                <Input />
              </Form.Item>

              <Form.Item name="dateOfBirth" label="Date of Birth"
                rules={[{ type: 'object' }]}
              >
                <DatePicker />
              </Form.Item>

              <Form.Item
                name="gender"
                label="Gender"
              >
                <Select placeholder="Please select a country">
                  <Option value="MALE">MALE</Option>
                  <Option value="FEMALE">FEMALE</Option>
                </Select>
              </Form.Item>

              <Form.Item
                name="plan"
                label="Plan"
              >
                <Radio.Group>
                  <Radio.Button value="a">Plan 1</Radio.Button>
                  <Radio.Button value="b">Plan 2</Radio.Button>
                  <Radio.Button value="c">Plan 3</Radio.Button>
                </Radio.Group>
              </Form.Item>

              <Form.Item
                requiredMark={'optional'}
                name="calculateType"
                label="Calculate Type"
                getValueFromEvent={(val) => {
                  setFormData((formData) => ({ ...formData, calculateType: val }))
                  return val
                }}
                rules={[
                  {
                    required: true,
                    message: 'Please select calculate type!',
                  },
                ]}
              >
                <Select placeholder="Please select calculate type">
                  <Option value="assured">Sum Assured</Option>
                  <Option value="premium">Premium Per Year</Option>
                </Select>
              </Form.Item>

              <Form.Item
                requiredMark={'optional'}
                name="premiumPerYear"
                label={`${formData.calculateType === "assured" ? "Sum Assured" : "Premium Per Year"}`}
                rules={[
                  {
                    required: true,
                    message: `Please select ${formData.calculateType === "assured" ? "Sum Assured" : "Premium Per Year"}!`,
                  },
                ]}
              >
                <InputNumber min={0} />
              </Form.Item>

              <Form.Item
                requiredMark={'optional'}
                name="paymentTerm"
                label="Payment Term"
                hasFeedback
                rules={[
                  {
                    required: true,
                    message: 'Please select payment term!',
                  },
                ]}
              >
                <Select placeholder="Please select payment term">
                  <Option value="YEARLY">YEARLY</Option>
                  <Option value="HALFYEARLY">HALFYEARLY</Option>
                  <Option value="QUARTERLY">QUARTERLY</Option>
                  <Option value="MONTHLY">MONTHLY</Option>
                </Select>
              </Form.Item>

              <Form.Item
                wrapperCol={{
                  span: 12,
                  offset: 6,
                }}
              >
                <Button type="primary" htmlType="submit">
                  Calculate
                </Button>
              </Form.Item>
            </Form>
            {resultProduct?.baseSumAssured > 0 && formData.calculateType === resultProduct.calculateType &&
              <Result
                icon={<SmileOutlined />}
                title={`${formData.calculateType === "assured" ? "Sum Assured" : "Premium Per Year"}: ${(resultProduct?.baseSumAssured).toFixed(2)}`}
              />
            }
          </Col>
        </Row>
      </Content>
    </Layout>
  );
}

export const onFinish = async (values, setFormData, getProduct) => {
  setFormData(formData => ({
    ...formData,
    ...values
  }));

  getProduct({
    genderCd: values.gender,
    dob: values.dateOfBirth,
    planCode: values.plan,
    paymentFrequency: values.paymentTerm,
    premiumPerYear: values.premiumPerYear,
    saPerYear: values.premiumPerYear,
    calculateType: values.calculateType,
  })
}

export const mapStateToProps = ({ ansurance }) => ({
  resultProduct: result(ansurance, 'resultProduct', {})
});

export const mapDispatchToProps = (dispatch) => ({
  getProduct: (data) => dispatch(getProduct(data))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);

